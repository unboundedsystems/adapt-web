---
id: cloud.docker.imagerefdockerhost.registry
title: "docker.ImageRefDockerHost.registry property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRefDockerHost](./cloud.docker.imagerefdockerhost.md) &gt; [registry](./cloud.docker.imagerefdockerhost.registry.md)

## docker.ImageRefDockerHost.registry property

<b>Signature:</b>

```typescript
readonly registry?: string;
```