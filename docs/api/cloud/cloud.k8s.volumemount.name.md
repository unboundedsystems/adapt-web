---
id: cloud.k8s.volumemount.name
title: "k8s.VolumeMount.name property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [VolumeMount](./cloud.k8s.volumemount.md) &gt; [name](./cloud.k8s.volumemount.name.md)

## k8s.VolumeMount.name property

This must match the Name of a Volume.

<b>Signature:</b>

```typescript
name: string;
```