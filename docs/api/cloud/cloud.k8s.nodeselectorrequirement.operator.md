---
id: cloud.k8s.nodeselectorrequirement.operator
title: "k8s.NodeSelectorRequirement.operator property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [NodeSelectorRequirement](./cloud.k8s.nodeselectorrequirement.md) &gt; [operator](./cloud.k8s.nodeselectorrequirement.operator.md)

## k8s.NodeSelectorRequirement.operator property

Represents a key's relationship to a set of values.

Valid operators are In, NotIn, Exists, DoesNotExist. Gt, and Lt.

<b>Signature:</b>

```typescript
operator: "In" | "NotIn" | "Exists" | "DoesNotExist" | "Gt" | "Lt";
```