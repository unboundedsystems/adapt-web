---
id: cloud.k8s.podprops.priority
title: "k8s.PodProps.priority property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [priority](./cloud.k8s.podprops.priority.md)

## k8s.PodProps.priority property

The priority various system components use this field to find the priority of the pod.

When Priority Admission Controller is enabled, it prevents users from setting this field. The admission controller populates this field from PriorityClassName. The higher the value, the higher the priority.

<b>Signature:</b>

```typescript
priority?: number;
```