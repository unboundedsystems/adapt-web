---
id: cloud.docker.imagerefregistry.tag
title: "docker.ImageRefRegistry.tag property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRefRegistry](./cloud.docker.imagerefregistry.md) &gt; [tag](./cloud.docker.imagerefregistry.tag.md)

## docker.ImageRefRegistry.tag property

<b>Signature:</b>

```typescript
readonly tag?: string;
```