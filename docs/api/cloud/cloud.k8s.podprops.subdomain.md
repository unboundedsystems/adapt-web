---
id: cloud.k8s.podprops.subdomain
title: "k8s.PodProps.subdomain property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [subdomain](./cloud.k8s.podprops.subdomain.md)

## k8s.PodProps.subdomain property

If specified, the fully qualified Pod hostname will be "<!-- -->&lt;<!-- -->hostname<!-- -->&gt;<!-- -->.<!-- -->&lt;<!-- -->subdomain<!-- -->&gt;<!-- -->.<!-- -->&lt;<!-- -->pod namespace<!-- -->&gt;<!-- -->.svc.<!-- -->&lt;<!-- -->cluster domain<!-- -->&gt;<!-- -->". If not specified, the pod will not have a domainname at all.

<b>Signature:</b>

```typescript
subdomain?: string;
```