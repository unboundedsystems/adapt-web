---
id: cloud.docker.imagerefdata.dockerhost
title: "docker.ImageRefData.dockerHost property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRefData](./cloud.docker.imagerefdata.md) &gt; [dockerHost](./cloud.docker.imagerefdata.dockerhost.md)

## docker.ImageRefData.dockerHost property

Docker host string to contact the Docker daemon where this image is located.

<b>Signature:</b>

```typescript
dockerHost?: "default" | string;
```

## Remarks

This should be in the same format that Docker expects for the `DOCKER_HOST` environment variable. The special string `default` can also be used, which will use the current value of the `DOCKER_HOST` environment variable, if it is set and will otherwise use the default named pipe on Windows (`npipe:///./pipe/docker_engine`<!-- -->) and the default socket on other systems (`unix:///var/run/docker.sock`<!-- -->).

## Example

tcp://localhost:2375
