---
id: cloud.k8s.ispod
title: "k8s.isPod() function"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [isPod](./cloud.k8s.ispod.md)

## k8s.isPod() function

Tests whether x is a Pod element

<b>Signature:</b>

```typescript
export declare function isPod(x: any): x is AdaptElement<PodProps>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  x | <code>any</code> | value to test |

<b>Returns:</b>

`x is AdaptElement<PodProps>`

`true` if x is a Pod element, false otherwise
