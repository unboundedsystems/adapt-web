---
id: cloud.k8s.probe.timeoutseconds
title: "k8s.Probe.timeoutSeconds property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [Probe](./cloud.k8s.probe.md) &gt; [timeoutSeconds](./cloud.k8s.probe.timeoutseconds.md)

## k8s.Probe.timeoutSeconds property

Number of seconds after which the probe times out.

Defaults to 1 second. Minimum value is 1.

More info: [https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle\#container-probes](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#container-probes)

<b>Signature:</b>

```typescript
timeoutSeconds?: number;
```