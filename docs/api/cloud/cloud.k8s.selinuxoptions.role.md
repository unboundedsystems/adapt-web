---
id: cloud.k8s.selinuxoptions.role
title: "k8s.SELinuxOptions.role property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [SELinuxOptions](./cloud.k8s.selinuxoptions.md) &gt; [role](./cloud.k8s.selinuxoptions.role.md)

## k8s.SELinuxOptions.role property

SELinux role label that applies to the container.

<b>Signature:</b>

```typescript
role: string;
```