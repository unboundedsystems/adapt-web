---
id: cloud.k8s.podaffinityterm
title: "k8s.PodAffinityTerm interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodAffinityTerm](./cloud.k8s.podaffinityterm.md)

## k8s.PodAffinityTerm interface


<b>Signature:</b>

```typescript
export interface PodAffinityTerm 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [labelSelector](./cloud.k8s.podaffinityterm.labelselector.md) | <code>LabelSelector</code> | Label query over a set of resources, in this case pods. |
|  [namespaces](./cloud.k8s.podaffinityterm.namespaces.md) | <code>string[]</code> | Specifies which namespaces the labelSelector applies to (matches against); null or empty list means "this pod's namespace" |
|  [topologyKey](./cloud.k8s.podaffinityterm.topologykey.md) | <code>string</code> | This pod should be co-located (affinity) or not co-located (anti-affinity) with the pods matching the labelSelector in the specified namespaces, where co-located is defined as running on a node whose value of the label with key topologyKey matches that of any node on which any of the selected pods is running. Empty topologyKey is not allowed. |
