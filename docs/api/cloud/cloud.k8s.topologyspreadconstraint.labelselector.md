---
id: cloud.k8s.topologyspreadconstraint.labelselector
title: "k8s.TopologySpreadConstraint.labelSelector property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [TopologySpreadConstraint](./cloud.k8s.topologyspreadconstraint.md) &gt; [labelSelector](./cloud.k8s.topologyspreadconstraint.labelselector.md)

## k8s.TopologySpreadConstraint.labelSelector property

Used to find matching pods.

Pods that match this label selector are counted to determine the number of pods in their corresponding topology domain.

<b>Signature:</b>

```typescript
labelSelector: LabelSelector;
```