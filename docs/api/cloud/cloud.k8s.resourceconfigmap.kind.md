---
id: cloud.k8s.resourceconfigmap.kind
title: "k8s.ResourceConfigMap.kind property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ResourceConfigMap](./cloud.k8s.resourceconfigmap.md) &gt; [kind](./cloud.k8s.resourceconfigmap.kind.md)

## k8s.ResourceConfigMap.kind property

<b>Signature:</b>

```typescript
kind: "ConfigMap";
```