---
id: cloud.k8s.configmapvolumesource.name
title: "k8s.ConfigMapVolumeSource.name property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ConfigMapVolumeSource](./cloud.k8s.configmapvolumesource.md) &gt; [name](./cloud.k8s.configmapvolumesource.name.md)

## k8s.ConfigMapVolumeSource.name property

Name of the referent.

More info: [https://kubernetes.io/docs/concepts/overview/working-with-objects/names/\#names](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names)

<b>Signature:</b>

```typescript
name: string | Handle;
```