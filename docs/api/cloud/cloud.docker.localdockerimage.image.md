---
id: cloud.docker.localdockerimage.image
title: "docker.LocalDockerImage.image() method"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [LocalDockerImage](./cloud.docker.localdockerimage.md) &gt; [image](./cloud.docker.localdockerimage.image.md)

## docker.LocalDockerImage.image() method

<b>Signature:</b>

```typescript
image(): WithId<ImageRefDockerHost> | undefined;
```
<b>Returns:</b>

`WithId<ImageRefDockerHost> | undefined`
