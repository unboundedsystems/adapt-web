---
id: cloud.k8s.volumemount.subpathexpr
title: "k8s.VolumeMount.subPathExpr property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [VolumeMount](./cloud.k8s.volumemount.md) &gt; [subPathExpr](./cloud.k8s.volumemount.subpathexpr.md)

## k8s.VolumeMount.subPathExpr property

Expanded path within the volume from which the container's volume should be mounted.

Behaves similarly to SubPath but environment variable references $(VAR\_NAME) are expanded using the container's environment.

Defaults to "" (volume's root).

SubPathExpr and SubPath are mutually exclusive.

<b>Signature:</b>

```typescript
subPathExpr?: string;
```