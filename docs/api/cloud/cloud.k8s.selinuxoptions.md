---
id: cloud.k8s.selinuxoptions
title: "k8s.SELinuxOptions interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [SELinuxOptions](./cloud.k8s.selinuxoptions.md)

## k8s.SELinuxOptions interface


<b>Signature:</b>

```typescript
export interface SELinuxOptions 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [level](./cloud.k8s.selinuxoptions.level.md) | <code>string</code> | SELinux level label that applies to the container. |
|  [role](./cloud.k8s.selinuxoptions.role.md) | <code>string</code> | SELinux role label that applies to the container. |
|  [type](./cloud.k8s.selinuxoptions.type.md) | <code>string</code> | SELinux type label that applies to the container. |
|  [user](./cloud.k8s.selinuxoptions.user.md) | <code>string</code> | SELinux user label that applies to the container |
