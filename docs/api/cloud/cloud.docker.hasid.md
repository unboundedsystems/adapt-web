---
id: cloud.docker.hasid
title: "docker.hasId() function"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [hasId](./cloud.docker.hasid.md)

## docker.hasId() function

Type guard for determining whether a given object has a non-null `id` property.

<b>Signature:</b>

```typescript
export declare function hasId<T extends {
    id?: ImageIdString;
}>(o: T): o is WithId<T>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  o | <code>T</code> |  |

<b>Returns:</b>

`o is WithId<T>`
