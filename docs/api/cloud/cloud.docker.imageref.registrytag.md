---
id: cloud.docker.imageref.registrytag
title: "docker.ImageRef.registryTag property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRef](./cloud.docker.imageref.md) &gt; [registryTag](./cloud.docker.imageref.registrytag.md)

## docker.ImageRef.registryTag property

The remote tag reference in `domain/path:tag` form.

<b>Signature:</b>

```typescript
readonly registryTag?: string;
```

## Remarks

Undefined if any of `domain`<!-- -->, `path`<!-- -->, or `tag` are unset.

## Example

gcr.io/my-project/image:1.0.1
