---
id: cloud.docker.dockerpushableimageinstance.pushto
title: "docker.DockerPushableImageInstance.pushTo() method"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [DockerPushableImageInstance](./cloud.docker.dockerpushableimageinstance.md) &gt; [pushTo](./cloud.docker.dockerpushableimageinstance.pushto.md)

## docker.DockerPushableImageInstance.pushTo() method

Pushes the image returned by `latestImage` to a Docker registry.

<b>Signature:</b>

```typescript
pushTo({ ref }: {
        ref: NameTagString;
    }): MaybePromise<ImageRefRegistry | undefined>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  { ref } | <code>{</code><br/><code>        ref: NameTagString;</code><br/><code>    }</code> |  |

<b>Returns:</b>

`MaybePromise<ImageRefRegistry | undefined>`

## Remarks

If there is no latest image available (`latestImage` returns undefined), then `pushTo` will return undefined. Otherwise, if the push was successful, returns an [docker.ImageRefRegistry](./cloud.docker.imagerefregistry.md) that contains the complete nameTag, including registry portion.
