---
id: cloud.k8s.podprops.imagepullsecrets
title: "k8s.PodProps.imagePullSecrets property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [imagePullSecrets](./cloud.k8s.podprops.imagepullsecrets.md)

## k8s.PodProps.imagePullSecrets property

List of references to secrets in the same namespace to use for pulling any of the images used by this PodSpec.

If specified, these secrets will be passed to individual puller implementations for them to use. For example, in the case of docker, only DockerConfig type secrets are honored. More info: [https://kubernetes.io/docs/concepts/containers/images\#specifying-imagepullsecrets-on-a-pod](https://kubernetes.io/docs/concepts/containers/images#specifying-imagepullsecrets-on-a-pod)

<b>Signature:</b>

```typescript
imagePullSecrets?: LocalObjectReference[];
```