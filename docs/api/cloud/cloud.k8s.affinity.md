---
id: cloud.k8s.affinity
title: "k8s.Affinity interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [Affinity](./cloud.k8s.affinity.md)

## k8s.Affinity interface


<b>Signature:</b>

```typescript
export interface Affinity 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [nodeAffinity](./cloud.k8s.affinity.nodeaffinity.md) | <code>NodeAffinity</code> | Describes node affinity scheduling rules for the pod. |
|  [podAffinity](./cloud.k8s.affinity.podaffinity.md) | <code>PodAffinity</code> | Describes pod affinity scheduling rules (e.g. co-locate this pod in the same node, zone, etc. as some other pod(s)). |
|  [podAntiAffinity](./cloud.k8s.affinity.podantiaffinity.md) | <code>PodAntiAffinity</code> | PodAntiAffinity Describes pod anti-affinity scheduling rules (e.g. avoid putting this pod in the same node, zone, etc. as some other pod(s)). |
