---
id: cloud.k8s.containerspec.volumemounts
title: "k8s.ContainerSpec.volumeMounts property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ContainerSpec](./cloud.k8s.containerspec.md) &gt; [volumeMounts](./cloud.k8s.containerspec.volumemounts.md)

## k8s.ContainerSpec.volumeMounts property

volumeDevices is the list of block devices to be used by the container.

<b>Signature:</b>

```typescript
volumeMounts?: VolumeMount[];
```