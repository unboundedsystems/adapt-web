---
id: cloud.docker.withid
title: "docker.WithId type"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [WithId](./cloud.docker.withid.md)

## docker.WithId type

Helper type that augments a base type to indicate that the `id` field is present and has a valid image ID.

<b>Signature:</b>

```typescript
export declare type WithId<T> = T & {
    id: ImageIdString;
};
```