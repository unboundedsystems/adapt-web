---
id: cloud.docker.buildkitimage.image
title: "docker.BuildKitImage.image() method"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [BuildKitImage](./cloud.docker.buildkitimage.md) &gt; [image](./cloud.docker.buildkitimage.image.md)

## docker.BuildKitImage.image() method

<b>Signature:</b>

```typescript
image(): WithId<ImageRefRegistry> | undefined;
```
<b>Returns:</b>

`WithId<ImageRefRegistry> | undefined`
