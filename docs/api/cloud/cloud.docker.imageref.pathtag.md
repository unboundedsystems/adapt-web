---
id: cloud.docker.imageref.pathtag
title: "docker.ImageRef.pathTag property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRef](./cloud.docker.imageref.md) &gt; [pathTag](./cloud.docker.imageref.pathtag.md)

## docker.ImageRef.pathTag property

The image path (not including any registry) and image tag. Returns undefined if either `path` or `tag` are not set.

<b>Signature:</b>

```typescript
readonly pathTag?: string;
```

## Example

my-project/image:1.0.1
