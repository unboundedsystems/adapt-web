---
id: cloud.k8s.podprops.priorityclassname
title: "k8s.PodProps.priorityClassName property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [priorityClassName](./cloud.k8s.podprops.priorityclassname.md)

## k8s.PodProps.priorityClassName property

If specified, indicates the pod's priority.

"system-node-critical" and "system-cluster-critical" are two special keywords which indicate the highest priorities with the former being the highest priority. Any other name must be defined by creating a PriorityClass object with that name. If not specified, the pod priority will be default or zero if there is no default.

<b>Signature:</b>

```typescript
priorityClassName?: string;
```