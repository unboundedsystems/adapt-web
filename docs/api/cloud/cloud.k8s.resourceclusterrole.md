---
id: cloud.k8s.resourceclusterrole
title: "k8s.ResourceClusterRole interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ResourceClusterRole](./cloud.k8s.resourceclusterrole.md)

## k8s.ResourceClusterRole interface


<b>Signature:</b>

```typescript
export interface ResourceClusterRole extends ResourceBase 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [aggregationRule](./cloud.k8s.resourceclusterrole.aggregationrule.md) | <code>AggregationRule</code> |  |
|  [apiVersion](./cloud.k8s.resourceclusterrole.apiversion.md) | <code>&quot;rbac.authorization.k8s.io/v1&quot;</code> |  |
|  [kind](./cloud.k8s.resourceclusterrole.kind.md) | <code>&quot;ClusterRole&quot;</code> |  |
|  [rules](./cloud.k8s.resourceclusterrole.rules.md) | <code>PolicyRule[]</code> |  |
