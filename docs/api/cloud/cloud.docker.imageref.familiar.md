---
id: cloud.docker.imageref.familiar
title: "docker.ImageRef.familiar property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRef](./cloud.docker.imageref.md) &gt; [familiar](./cloud.docker.imageref.familiar.md)

## docker.ImageRef.familiar property

The complete string reference for this image in familiar form, which leaves out certain fields from the reference when they are set to default values.

<b>Signature:</b>

```typescript
readonly familiar?: string;
```

## Remarks

This reference form is the form typically used in the Docker UI. If the ImageRef is not complete, familiar will return `undefined`<!-- -->, as the correct familiar representation cannot be determined.

## Example 1

mysql

## Example 2

gcr.io/my-project/image:1.0.1
