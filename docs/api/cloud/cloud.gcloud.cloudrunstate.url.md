---
id: cloud.gcloud.cloudrunstate.url
title: "gcloud.CloudRunState.url property"
hide_title: true
parent_id: api/cloud/cloud.gcloud
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [gcloud](./cloud.gcloud.md) &gt; [CloudRunState](./cloud.gcloud.cloudrunstate.md) &gt; [url](./cloud.gcloud.cloudrunstate.url.md)

## gcloud.CloudRunState.url property

URL for the service the last time it was queried (may not be current).

<b>Signature:</b>

```typescript
url?: string;
```