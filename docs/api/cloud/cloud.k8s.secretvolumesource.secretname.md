---
id: cloud.k8s.secretvolumesource.secretname
title: "k8s.SecretVolumeSource.secretName property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [SecretVolumeSource](./cloud.k8s.secretvolumesource.md) &gt; [secretName](./cloud.k8s.secretvolumesource.secretname.md)

## k8s.SecretVolumeSource.secretName property

Name of the secret in the pod's namespace to use.

More info: [https://kubernetes.io/docs/concepts/storage/volumes\#secret](https://kubernetes.io/docs/concepts/storage/volumes#secret)

<b>Signature:</b>

```typescript
secretName: string | Handle;
```