---
id: cloud.k8s.podprops.hostpid
title: "k8s.PodProps.hostPID property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [hostPID](./cloud.k8s.podprops.hostpid.md)

## k8s.PodProps.hostPID property

Use the host's pid namespace.

<b>Signature:</b>

```typescript
hostPID: boolean;
```