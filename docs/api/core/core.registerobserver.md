---
id: core.registerobserver
title: "registerObserver() function"
hide_title: true
parent_id: api/core/core
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Core API Overview](overview) &gt; [@adpt/core](./core.md) &gt; [registerObserver](./core.registerobserver.md)

## registerObserver() function

<b>Signature:</b>

```typescript
export declare function registerObserver<D = object, C = any>(obs: ObserverPlugin<D, C>, nameIn?: string): string;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  obs | [<code>ObserverPlugin</code>](./core.observerplugin.md)<code>&lt;D, C&gt;</code> |  |
|  nameIn | <code>string</code> |  |

<b>Returns:</b>

`string`
