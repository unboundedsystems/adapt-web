---
id: cloud.docker.imagerefregistry.pathtag
title: "docker.ImageRefRegistry.pathTag property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRefRegistry](./cloud.docker.imagerefregistry.md) &gt; [pathTag](./cloud.docker.imagerefregistry.pathtag.md)

## docker.ImageRefRegistry.pathTag property

<b>Signature:</b>

```typescript
readonly pathTag?: string;
```