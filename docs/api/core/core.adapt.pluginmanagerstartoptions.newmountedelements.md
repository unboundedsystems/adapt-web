---
id: core.adapt.pluginmanagerstartoptions.newmountedelements
title: "Adapt.PluginManagerStartOptions.newMountedElements property"
hide_title: true
parent_id: api/core/core.adapt
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Core API Overview](overview) &gt; [@adpt/core](./core.md) &gt; [Adapt](./core.adapt.md) &gt; [PluginManagerStartOptions](./core.adapt.pluginmanagerstartoptions.md) &gt; [newMountedElements](./core.adapt.pluginmanagerstartoptions.newmountedelements.md)

## Adapt.PluginManagerStartOptions.newMountedElements property

All mountedElements from build of the new DOM

<b>Signature:</b>

```typescript
newMountedElements: AdaptMountedElement[];
```