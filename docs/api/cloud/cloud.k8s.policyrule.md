---
id: cloud.k8s.policyrule
title: "k8s.PolicyRule interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PolicyRule](./cloud.k8s.policyrule.md)

## k8s.PolicyRule interface


<b>Signature:</b>

```typescript
export interface PolicyRule 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [apiGroups](./cloud.k8s.policyrule.apigroups.md) | <code>string[]</code> | APIGroups is the name of the APIGroup that contains the resources.<!-- -->If multiple API groups are specified, any action requested against one of the enumerated resources in any API group will be allowed. |
|  [nonResourceURLs](./cloud.k8s.policyrule.nonresourceurls.md) | <code>string[]</code> | NonResourceURLs is a set of partial urls that a user should have access to.<!-- -->\*s are allowed, but only as the full, final step in the path. Since non-resource URLs are not namespaced, this field is only applicable for ClusterRoles referenced from a ClusterRoleBinding. Rules can either apply to API resources (such as "pods" or "secrets") or non-resource URL paths (such as "/api"), but not both. |
|  [resourceNames](./cloud.k8s.policyrule.resourcenames.md) | <code>string[]</code> | ResourceNames is an optional white list of names that the rule applies to.<!-- -->An empty set means that everything is allowed. |
|  [resources](./cloud.k8s.policyrule.resources.md) | <code>string[]</code> | Resources is a list of resources this rule applies to.<!-- -->ResourceAll represents all resources. |
|  [verbs](./cloud.k8s.policyrule.verbs.md) | <code>string[]</code> | Verbs is a list of Verbs that apply to ALL the ResourceKinds and AttributeRestrictions contained in this rule.<!-- -->VerbAll represents all kinds. |
