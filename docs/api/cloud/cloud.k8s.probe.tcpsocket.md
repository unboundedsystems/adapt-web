---
id: cloud.k8s.probe.tcpsocket
title: "k8s.Probe.tcpSocket property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [Probe](./cloud.k8s.probe.md) &gt; [tcpSocket](./cloud.k8s.probe.tcpsocket.md)

## k8s.Probe.tcpSocket property

Specifies an action involving a TCP port.

TCP hooks not yet supported

<b>Signature:</b>

```typescript
tcpSocket?: TCPSocketAction;
```