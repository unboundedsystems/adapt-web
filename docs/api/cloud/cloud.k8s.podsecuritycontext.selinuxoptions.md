---
id: cloud.k8s.podsecuritycontext.selinuxoptions
title: "k8s.PodSecurityContext.seLinuxOptions property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodSecurityContext](./cloud.k8s.podsecuritycontext.md) &gt; [seLinuxOptions](./cloud.k8s.podsecuritycontext.selinuxoptions.md)

## k8s.PodSecurityContext.seLinuxOptions property

The SELinux context to be applied to all containers.

If unspecified, the container runtime will allocate a random SELinux context for each container. May also be set in SecurityContext. If set in both SecurityContext and PodSecurityContext, the value specified in SecurityContext takes precedence for that container.

<b>Signature:</b>

```typescript
seLinuxOptions?: SELinuxOptions;
```