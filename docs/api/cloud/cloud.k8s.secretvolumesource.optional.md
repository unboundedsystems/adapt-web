---
id: cloud.k8s.secretvolumesource.optional
title: "k8s.SecretVolumeSource.optional property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [SecretVolumeSource](./cloud.k8s.secretvolumesource.md) &gt; [optional](./cloud.k8s.secretvolumesource.optional.md)

## k8s.SecretVolumeSource.optional property

Specify whether the Secret or its keys must be defined

<b>Signature:</b>

```typescript
optional?: boolean;
```