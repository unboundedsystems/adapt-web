---
id: cloud.k8s.isclusterroleprops
title: "k8s.isClusterRoleProps() function"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [isClusterRoleProps](./cloud.k8s.isclusterroleprops.md)

## k8s.isClusterRoleProps() function


<b>Signature:</b>

```typescript
export declare function isClusterRoleProps(props: ResourceProps): props is ResourceProps & ResourceClusterRole;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  props | <code>ResourceProps</code> |  |

<b>Returns:</b>

`props is ResourceProps & ResourceClusterRole`
