---
id: core.adapt.pluginmanagerstartoptions.newdom
title: "Adapt.PluginManagerStartOptions.newDom property"
hide_title: true
parent_id: api/core/core.adapt
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Core API Overview](overview) &gt; [@adpt/core](./core.md) &gt; [Adapt](./core.adapt.md) &gt; [PluginManagerStartOptions](./core.adapt.pluginmanagerstartoptions.md) &gt; [newDom](./core.adapt.pluginmanagerstartoptions.newdom.md)

## Adapt.PluginManagerStartOptions.newDom property

The new DOM to be deployed

<b>Signature:</b>

```typescript
newDom: AdaptElementOrNull;
```