---
id: cloud.k8s.podprops.activedeadlineseconds
title: "k8s.PodProps.activeDeadlineSeconds property"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [PodProps](./cloud.k8s.podprops.md) &gt; [activeDeadlineSeconds](./cloud.k8s.podprops.activedeadlineseconds.md)

## k8s.PodProps.activeDeadlineSeconds property

Optional duration in seconds the pod may be active on the node relative to StartTime before the system will actively try to mark it failed and kill associated containers.

Value must be a positive integer.

<b>Signature:</b>

```typescript
activeDeadlineSeconds?: number;
```