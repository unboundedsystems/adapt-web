---
id: cloud.k8s.resourcesecret
title: "k8s.ResourceSecret interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ResourceSecret](./cloud.k8s.resourcesecret.md)

## k8s.ResourceSecret interface


<b>Signature:</b>

```typescript
export interface ResourceSecret extends ResourceBase 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [data](./cloud.k8s.resourcesecret.data.md) | <code>{</code><br/><code>        [key: string]: string;</code><br/><code>    }</code> |  |
|  [kind](./cloud.k8s.resourcesecret.kind.md) | <code>&quot;Secret&quot;</code> |  |
|  [stringData](./cloud.k8s.resourcesecret.stringdata.md) | <code>{</code><br/><code>        [key: string]: string;</code><br/><code>    }</code> |  |
|  [type](./cloud.k8s.resourcesecret.type.md) | <code>string</code> |  |
