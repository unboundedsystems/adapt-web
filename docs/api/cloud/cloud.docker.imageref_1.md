---
id: cloud.docker.imageref_1
title: "docker.imageRef() function"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [imageRef](./cloud.docker.imageref_1.md)

## docker.imageRef() function

Factory function for creating a [docker.ImageRef](./cloud.docker.imageref.md) or cloning an existing one.

<b>Signature:</b>

```typescript
export declare function imageRef(info?: ImageRefData): ImageRef;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  info | <code>ImageRefData</code> |  |

<b>Returns:</b>

`ImageRef`
