---
id: cloud.k8s.sysctl
title: "k8s.Sysctl interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [Sysctl](./cloud.k8s.sysctl.md)

## k8s.Sysctl interface


<b>Signature:</b>

```typescript
export interface Sysctl 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [name](./cloud.k8s.sysctl.name.md) | <code>string</code> |  |
|  [value](./cloud.k8s.sysctl.value.md) | <code>string</code> |  |
