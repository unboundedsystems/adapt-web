---
id: cloud.docker.imagerefdockerhost.familiar
title: "docker.ImageRefDockerHost.familiar property"
hide_title: true
parent_id: api/cloud/cloud.docker
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [docker](./cloud.docker.md) &gt; [ImageRefDockerHost](./cloud.docker.imagerefdockerhost.md) &gt; [familiar](./cloud.docker.imagerefdockerhost.familiar.md)

## docker.ImageRefDockerHost.familiar property

<b>Signature:</b>

```typescript
readonly familiar: string;
```