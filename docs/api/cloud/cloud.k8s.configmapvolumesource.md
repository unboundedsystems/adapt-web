---
id: cloud.k8s.configmapvolumesource
title: "k8s.ConfigMapVolumeSource interface"
hide_title: true
parent_id: api/cloud/cloud.k8s
---
<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Cloud API Overview](overview) &gt; [@adpt/cloud](./cloud.md) &gt; [k8s](./cloud.k8s.md) &gt; [ConfigMapVolumeSource](./cloud.k8s.configmapvolumesource.md)

## k8s.ConfigMapVolumeSource interface


<b>Signature:</b>

```typescript
export interface ConfigMapVolumeSource 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [defaultMode](./cloud.k8s.configmapvolumesource.defaultmode.md) | <code>number</code> | mode bits to use on created files by default.<!-- -->Must be a value between 0 and 0777. Defaults to 0644. Directories within the path are not affected by this setting. This might be in conflict with other options that affect the file mode, like fsGroup, and the result can be other mode bits set. |
|  [items](./cloud.k8s.configmapvolumesource.items.md) | <code>KeyToPath[]</code> | If unspecified, each key-value pair in the Data field of the referenced ConfigMap will be projected into the volume as a file whose name is the key and content is the value.<!-- -->If specified, the listed keys will be projected into the specified paths, and unlisted keys will not be present. If a key is specified which is not present in the ConfigMap, the volume setup will error unless it is marked optional. Paths must be relative and may not contain the '..' path or start with '..'. |
|  [name](./cloud.k8s.configmapvolumesource.name.md) | <code>string &#124; Handle</code> | Name of the referent.<!-- -->More info: [https://kubernetes.io/docs/concepts/overview/working-with-objects/names/\#names](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names) |
|  [optional](./cloud.k8s.configmapvolumesource.optional.md) | <code>boolean</code> | Specify whether the ConfigMap or its keys must be defined |
